package edu.ntnu.idatt2001.arbeidskrav4.Model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class DeckOfCardsTest {

  @Nested
  public class ConstructorTest {
    @Test
    public void checkForNoDuplicates() {
      boolean duplicates = false;
      DeckOfCards deckOfCCards = new DeckOfCards();
      HashSet<PlayingCard> duplicatesOfCards = new HashSet<>();
      for (PlayingCard card : deckOfCCards.getCards()) {
        if (!duplicatesOfCards.add(card)) {
          duplicates = true;
        }
      }
      assertFalse(duplicates);
    }
  }
  
  @Nested
  public class Methods {
    @Test
    public void getCardTest() {
      DeckOfCards deckOfCards = new DeckOfCards();
      assertEquals('H', deckOfCards.getCard("H10").getSuit());
      assertEquals(10, deckOfCards.getCard("H10").getFace());
    }
    
    @Test
    public void dealHandTest1() {
      DeckOfCards deckOfCards = new DeckOfCards();
      deckOfCards.dealHand(10);
      assertEquals(42, deckOfCards.getCards().size());
    }
  
    @Test
    public void dealHandTest2() {
      DeckOfCards deckOfCards = new DeckOfCards();
      deckOfCards.dealHand(25);
      assertEquals(27, deckOfCards.getCards().size());
    }
    
    @Test
    public void retrieveAllCardsTest() {
      DeckOfCards deckOfCards = new DeckOfCards();
      deckOfCards.dealHand(25);
      deckOfCards.retrieveAllCards();
      assertEquals(52, deckOfCards.getCards().size());
    }
  }

}
