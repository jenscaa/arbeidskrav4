package edu.ntnu.idatt2001.arbeidskrav4.Model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
public class PlayingCardTest {
  private PlayingCard playingCard;
  
  @BeforeEach
  public void setUp() {
    playingCard = new PlayingCard('H', 13);
  }
  
  @Nested
  public class MethodTest {
    @Test
    public void getAsStringTest() {
      assertEquals("H13", playingCard.getAsString());
    }
  
    @Test
    public void getSuitTest() {
      assertEquals('H', playingCard.getSuit());
    }
  
    @Test
    public void getFaceTest() {
      assertEquals(13, playingCard.getFace());
    }
  }
}
