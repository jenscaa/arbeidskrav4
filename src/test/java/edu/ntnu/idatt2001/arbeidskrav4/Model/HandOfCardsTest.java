package edu.ntnu.idatt2001.arbeidskrav4.Model;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

public class HandOfCardsTest {
  private List<PlayingCard> cards;
  
  @BeforeEach
  public void setUp() {
    cards = List.of(new PlayingCard('H',1 ),
            new PlayingCard('H',2 ),
            new PlayingCard('H',4 ),
            new PlayingCard('H',7 ),
            new PlayingCard('H',13));
  }
  
  @Nested
  public class ConstructorTest {
    @Test
    public void constructorWithParameterTest() {
      HandOfCards hand = new HandOfCards(cards);
      assertEquals(5, hand.getCards().size());
    }
    
  }
  @Nested
  public class MethodTest {
    @Test
    public void emptyHandTest() {
      HandOfCards hand = new HandOfCards(cards);
      hand.emptyHand();
      assertEquals(0, hand.getCards().size());
    }
    
    @Test
    public void pickNewHandTest() {
      HandOfCards hand = new HandOfCards(cards);
      List<PlayingCard> cards2 = List.of(new PlayingCard('S',1 ),
              new PlayingCard('S',2 ),
              new PlayingCard('S',4 ));
      hand.pickNewHand(cards2);
      assertEquals(3, hand.getCards().size());
    }
    
    @Test
    public void checkForFlushTest() {
      HandOfCards hand = new HandOfCards(cards);
      assertTrue(hand.checkFor5CardFlush());
    }
  }
}
