package edu.ntnu.idatt2001.arbeidskrav4.Controller;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.arbeidskrav4.Model.HandOfCards;
import edu.ntnu.idatt2001.arbeidskrav4.Model.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.util.List;

public class PokerControllerTest {
  private final PokerController pokerController = new PokerController();
  private List<PlayingCard> cards;
  
  @BeforeEach
  public void setUp() {
    cards = List.of(new PlayingCard('H', 1),
            new PlayingCard('H', 2),
            new PlayingCard('H', 4),
            new PlayingCard('H', 7),
            new PlayingCard('H', 13));
  }
    @Nested
    public class MethodTest {
      @Test
      public void getSumOfFacesTest() {
        HandOfCards hand = new HandOfCards(cards);
        assertEquals(27, pokerController.getSumOfFaces(hand));
      }
  
      @Test
      public void checkIfFlushTest() {
        HandOfCards hand = new HandOfCards(cards);
        assertEquals("Yes", pokerController.flush(hand));
      }
  
      @Test
      public void getCardsOfHeartsTest() {
        HandOfCards hand = new HandOfCards(cards);
        assertEquals("H1 H2 H4 H7 H13 ", pokerController.getCardsOfHearts(hand));
      }
  
      @Test
      public void getQueenOfSpadesFailTest() {
        HandOfCards hand = new HandOfCards(cards);
        assertEquals("No", pokerController.getQueenOfSpades(hand));
      }
  
      @Test
      public void getQueenOfSpadesTest() {
        HandOfCards hand = new HandOfCards(cards);
        List<PlayingCard> cards2 = List.of(new PlayingCard('S',1 ),
                new PlayingCard('S',2 ),
                new PlayingCard('S',12 ));
        hand.pickNewHand(cards2);
        assertEquals("Yes", pokerController.getQueenOfSpades(hand));
      }
    }
}
