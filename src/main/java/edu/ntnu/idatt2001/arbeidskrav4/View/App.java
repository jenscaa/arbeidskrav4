package edu.ntnu.idatt2001.arbeidskrav4.View;

/**
 * Runs the Poker Application
 *
 * @author jensc
 */
public class App {
  public static void main(String[] args) {
    Poker.main(args);
  }
}
