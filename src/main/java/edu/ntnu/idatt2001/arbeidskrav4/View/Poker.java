package edu.ntnu.idatt2001.arbeidskrav4.View;

import edu.ntnu.idatt2001.arbeidskrav4.Controller.PokerController;
import edu.ntnu.idatt2001.arbeidskrav4.Model.DeckOfCards;
import edu.ntnu.idatt2001.arbeidskrav4.Model.HandOfCards;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.List;

/**
 * View Class for the Poker Application - Arbeidskrav 4
 *
 * @author jensc
 */
public class Poker extends Application {
  public static void main(String[] args) {
    launch(args);
  }
  
  private PokerController controller;
  private final DeckOfCards cards = new DeckOfCards();
  private final HandOfCards hand = new HandOfCards();
  
  @Override
  public void start(Stage primaryStage) {
    this.controller = new PokerController();
    
    //Background
    ImageView backgroundImg = new ImageView(controller.getCardImage("greenBackground"));
    backgroundImg.setFitHeight(700);
    backgroundImg.setFitWidth(700);
  
    //Layout
    StackPane root = new StackPane();
    
    //Scene
    Scene scene = new Scene(root, 700, 700);
  
    primaryStage.setTitle("Poker");
    primaryStage.setResizable(false);
    primaryStage.setScene(scene);
    primaryStage.show();
    
    //Label if there are no more cards
    Label noMoreCards = new Label("There are no more cards to deal out. Start over");
    noMoreCards.setTextFill(Color.web("#FFFFFF"));
    
    //Label to count card left in the deck
    Label cardsLeft = new Label("Cards left: " + cards.getCards().size());
    cardsLeft.setTextFill(Color.web("#FFFFFF"));
    
    //Label: sum of faces
    Label sumOfFaces = new Label("Sum of faces: " + controller.getSumOfFaces(hand));
    sumOfFaces.setTextFill(Color.web("#FFFFFF"));
    
    //Label: flush
    Label flushLabel = new Label("Flush: " + controller.flush(hand));
    flushLabel.setTextFill(Color.web("#FFFFFF"));
    
    //Label Cards of hearts
    Label cardsOfHearts = new Label("Cards of hearts: " + controller.getCardsOfHearts(hand));
    cardsOfHearts.setTextFill(Color.web("#FFFFFF"));
    
    //Label Queen of spades
    Label queenOfSpades = new Label("Queen of spades: " + controller.getQueenOfSpades(hand));
    queenOfSpades.setTextFill(Color.web("#FFFFFF"));
    
    //Deck cards (Upside-down)
    ImageView Deck = new ImageView(controller.getCardImage("Backside"));
    Deck.setFitWidth(60);
    Deck.setFitHeight(100);
    
    //Card 1, 2, 3, 4, 5
    ImageView card1 = new ImageView(controller.getCardImage("Backside"));
    ImageView card2 = new ImageView(controller.getCardImage("Backside"));
    ImageView card3 = new ImageView(controller.getCardImage("Backside"));
    ImageView card4 = new ImageView(controller.getCardImage("Backside"));
    ImageView card5 = new ImageView(controller.getCardImage("Backside"));
    
    List<ImageView> list = List.of(card1, card2, card3, card4, card5);
    list.forEach(card -> {
      card.setFitWidth(60);
      card.setFitHeight(100);
    });
    
    //Adding movement transition from deck to hand
    TranslateTransition transitionToHandCard1 = new TranslateTransition(Duration.millis(500));
    TranslateTransition transitionToHandCard2 = new TranslateTransition(Duration.millis(500));
    TranslateTransition transitionToHandCard3 = new TranslateTransition(Duration.millis(500));
    TranslateTransition transitionToHandCard4 = new TranslateTransition(Duration.millis(500));
    TranslateTransition transitionToHandCard5 = new TranslateTransition(Duration.millis(500));
    
    transitionToHandCard1.setNode(card1);
    transitionToHandCard2.setNode(card2);
    transitionToHandCard3.setNode(card3);
    transitionToHandCard4.setNode(card4);
    transitionToHandCard5.setNode(card5);
  
    //Adding rotation transition from deck to hand
    RotateTransition rotateTransitionCard1 = new RotateTransition(Duration.millis(69));
    RotateTransition rotateTransitionCard2 = new RotateTransition(Duration.millis(69));
    RotateTransition rotateTransitionCard3 = new RotateTransition(Duration.millis(69));
    RotateTransition rotateTransitionCard4 = new RotateTransition(Duration.millis(69));
    RotateTransition rotateTransitionCard5 = new RotateTransition(Duration.millis(69));
    
    rotateTransitionCard1.setNode(card1);
    rotateTransitionCard2.setNode(card2);
    rotateTransitionCard3.setNode(card3);
    rotateTransitionCard4.setNode(card4);
    rotateTransitionCard5.setNode(card5);
    
    
    
    //Start over button
    Button startOver = new Button("Start Over");
    startOver.setOnAction(action -> {
      controller.startOver(cards, sumOfFaces, cardsLeft,
              hand, noMoreCards, transitionToHandCard1,
              transitionToHandCard2, transitionToHandCard3,
              transitionToHandCard4, transitionToHandCard5);
    });
  
    //Check hand button
    Button checkHand = new Button("Check hand");
    checkHand.setOnAction(action -> {
      controller.checkHand(cardsLeft, cards, noMoreCards,
              hand, sumOfFaces, flushLabel, cardsOfHearts, queenOfSpades,
              card1, card2, card3, card4, card5, rotateTransitionCard1, rotateTransitionCard2,
              rotateTransitionCard3, rotateTransitionCard4, rotateTransitionCard5);
    });
    
    //Deal hand button
    Button dealHand = new Button("Deal hand");
    dealHand.setOnAction(event -> {
      controller.dealHand(hand, cardsLeft, cards, card1, card2, card3, card4, card5,
              transitionToHandCard1, transitionToHandCard2, transitionToHandCard3,
              transitionToHandCard4, transitionToHandCard5);
    });
    
    //Adds elements to the root
    root.getChildren().add(backgroundImg);
    root.getChildren().add(noMoreCards);
    root.getChildren().add(cardsLeft);
    root.getChildren().add(sumOfFaces);
    root.getChildren().add(cardsOfHearts);
    root.getChildren().add(flushLabel);
    root.getChildren().add(queenOfSpades);
    root.getChildren().add(checkHand);
    root.getChildren().add(dealHand);
    root.getChildren().add(startOver);
    root.getChildren().add(Deck);
    StackPane.setAlignment(Deck, Pos.TOP_CENTER);
    StackPane.setAlignment(sumOfFaces, Pos.TOP_CENTER);
    StackPane.setAlignment(noMoreCards, Pos.TOP_CENTER);
    StackPane.setAlignment(cardsLeft, Pos.TOP_CENTER);
    StackPane.setAlignment(flushLabel, Pos.TOP_CENTER);
    StackPane.setAlignment(cardsOfHearts, Pos.TOP_CENTER);
    StackPane.setAlignment(queenOfSpades, Pos.TOP_CENTER);
    list.forEach(card -> {
      root.getChildren().add(card);
      StackPane.setAlignment(card, Pos.TOP_CENTER);
    });
    
    //Fix positioning of buttons
    queenOfSpades.setTranslateX(0);
    queenOfSpades.setTranslateY(650);
    cardsOfHearts.setTranslateX(0);
    cardsOfHearts.setTranslateY(600);
    flushLabel.setTranslateX(-213);
    flushLabel.setTranslateY(650);
    sumOfFaces.setTranslateX(-200);
    sumOfFaces.setTranslateY(600);
    cardsLeft.setTranslateX(0);
    cardsLeft.setTranslateY(100);
    noMoreCards.setTranslateX(0);
    noMoreCards.setTranslateY(-100);
    dealHand.setTranslateX(270);
    dealHand.setTranslateY(-190);
    checkHand.setTranslateX(270);
    checkHand.setTranslateY(-100);
    startOver.setTranslateX(270);
    startOver.setTranslateY(-280);
  }
}