package edu.ntnu.idatt2001.arbeidskrav4.Model;

import java.util.*;

/**
 * Represents a deck of every 52.
 * The class HashMap is used to hold all the cards.
 *
 * @author Jens Christian Aanestad
 * @version 0.1
 */
public class DeckOfCards {
  private static final char[] suit = { 'S', 'H', 'D', 'C' };
  private final Map<String, PlayingCard> cards = new HashMap<>();
  
  /**
   * Initializes all 52 cards.
   * Uses two for-loops to generate all 13 cards
   * from the different suits.
   */
  public DeckOfCards() {
    for (char type : suit) {
      for (int face = 1; face <= 13; face++) {
        PlayingCard playingCard = new PlayingCard(type, face);
        cards.put(playingCard.getAsString(), playingCard);
      }
    }
  }
  
  /**
   * Returns a collection of all the cards
   *
   * @return Collection - a collection of the cards
   */
  public Collection<PlayingCard> getCards() {
    return cards.values();
  }
  
  /**
   * Returns the card associated with given abbreviation.
   * For example the abbreviation for 4 of hearts is H4.
   * If the abbreviation is nor valid, the method returns null
   *
   * @param card String - the abbreviation for a card.
   * @return PlayingCard - a chosen card from the deck
   */
  public PlayingCard getCard(String card) {
    return cards.get(card);
  }
  
  /**
   * Retrieves all cards back to the cards list.
   */
  public void retrieveAllCards() {
    cards.clear();
    for (char type : suit) {
      for (int face = 1; face <= 13; face++) {
        PlayingCard playingCard = new PlayingCard(type, face);
        cards.put(playingCard.getAsString(), playingCard);
      }
    }
  }
  
  /**
   * Returns a list which picks random cards from
   * the deck of cards. Deck will lose the card which is
   * randomly picked.
   *
   * @param n int - how many cards to pick from the deck
   * @return List - a list of all cards picked
   * @throws IllegalArgumentException if trying to deal more cards
   * than what is already in deck
   */
  public List<PlayingCard> dealHand(int n) throws IllegalArgumentException {
    if (n > cards.size()) {
      throw new IllegalArgumentException("Can not deal out more cards than the amount of cards left");
    }
    if (n < 0) {
      throw new IllegalArgumentException("Can not deal out negative number of cards");
    }
    Random random = new Random();
    List<PlayingCard> newList = new ArrayList<>();
    List<String> cardKeys = new ArrayList<>(cards.keySet());
    for (int i = 0; i < n; i++) {
      int randomIndex = random.nextInt(cardKeys.size());
      newList.add(cards.get(cardKeys.get(randomIndex)));
      cards.remove(cardKeys.get(randomIndex));
      cardKeys.remove(randomIndex);
    }
    return newList;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeckOfCards that = (DeckOfCards) o;
    return cards.keySet().containsAll(that.cards.keySet());
  }
}
