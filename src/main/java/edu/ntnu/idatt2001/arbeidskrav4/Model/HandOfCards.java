package edu.ntnu.idatt2001.arbeidskrav4.Model;

import java.util.*;

/**
 * Represents a hand of cards.
 * Uses the class HashMap to store the cards.
 *
 * @author jensc
 * @version 0.1
 */
public class HandOfCards {
  private static final char[] suit = { 'S', 'H', 'D', 'C' };
  private final Map<String, PlayingCard> cards;
  
  /**
   * Initializes the hand of cards with a list.
   *
   * @param cards List - the list of the hand's cards
   */
  public HandOfCards(List<PlayingCard> cards) {
    this.cards = new HashMap<>();
    for (PlayingCard card : cards) {
      this.cards.put(card.getAsString(), card);
    }
  }
  
  /**
   * Initializes the hand of cards with an e list.
   */
  public HandOfCards() {
    this.cards = new HashMap<>();
  }
  
  /**
   * Returns a list of all playing cards belonging to the hand.
   *
   * @return ArrayList - a list of playing cards
   */
  public ArrayList<PlayingCard> getCards() {
    ArrayList<PlayingCard> newCards = new ArrayList<>();
    newCards.addAll(cards.values());
    return newCards;
  }
  
  /**
   * Clears out and picks a new hand.
   *
   * @param newCards List - list of new cards
   */
  public void pickNewHand(List<PlayingCard> newCards) {
    cards.clear();
    for (PlayingCard card : newCards) {
      cards.put(card.getAsString(), card);
    }
  }
  
  /**
   * Clear out all cards on the hand.
   */
  public void emptyHand() {
    cards.clear();
  }
  
  /**
   * Returns chosen card from the hand. The card that
   * is played will then be removed from the hand.
   * If the abbreviation for the card is not valid, then
   * it returns null.
   *
   * @param card String - the abbreviation for the card
   * @return PlayingCard - the chosen card.
   */
  public PlayingCard playCard(String card) {
    PlayingCard returnCard = cards.get(card);
    cards.remove(card);
    return returnCard;
  }
  
  /**
   * Returns a boolean if the hand has a flush.
   *
   * @return boolean - true if hand has flush
   */
  public boolean checkFor5CardFlush() {
    return (cards.values().stream().allMatch(suit -> suit.getSuit() == 'S')
            || cards.values().stream().allMatch(suit -> suit.getSuit() == 'H'))
            || cards.values().stream().allMatch(suit -> suit.getSuit() == 'D')
            || cards.values().stream().allMatch(suit -> suit.getSuit() == 'C');
  }
}
