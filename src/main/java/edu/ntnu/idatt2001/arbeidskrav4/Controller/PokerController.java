package edu.ntnu.idatt2001.arbeidskrav4.Controller;

import edu.ntnu.idatt2001.arbeidskrav4.Model.DeckOfCards;
import edu.ntnu.idatt2001.arbeidskrav4.Model.HandOfCards;
import edu.ntnu.idatt2001.arbeidskrav4.Model.PlayingCard;
import edu.ntnu.idatt2001.arbeidskrav4.View.Poker;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller class for the Poker Application - Arbeidskrav 4
 *
 * @author jensc
 */
public class PokerController {
  private Boolean dealCards = false;
  private Boolean handGiven = false;
  
  /**
   * Returns the card image.
   *
   * @param card String - The abbreviation to the card
   * @return Image - an image representing the card
   */
  public Image getCardImage(String card) {
    Image image = null;
    try {
      image = new Image(new FileInputStream("src//main//resources//" + card + ".PNG"));
    } catch (FileNotFoundException ex) {
      Logger.getLogger(Poker.class.getName()).log(Level.SEVERE, null, ex);
    }
    return image;
  }
  
  /**
   * Returns the sum of all faces belonging to given hand.
   *
   * @param hand HandOfCards
   * @return int - the sum of all faces
   */
  public int getSumOfFaces(HandOfCards hand) {
    if (hand.getCards().size() == 0) {
      return 0;
    }
    int sum = 0;
    sum = hand.getCards().stream().map(PlayingCard::getFace).reduce(Integer::sum).get();
    return sum;
  }
  
  /**
   * Checks if the given hand has a flush.
   *
   * @param hand HandOfCards
   * @return String - Returns "Yes" if hand has flush, returns "No" if not
   */
  public String flush(HandOfCards hand) {
    if (hand.getCards().size() == 0) {
      return "No";
    }
    if (hand.checkFor5CardFlush()) {
      return "Yes";
    } else {
      return "No";
    }
  }
  
  /**
   * Returns a string with abbreviations of cards with heart suit.
   *
   * @param hand HandOfCards
   * @return String - text with all cards of heart suit
   */
  public String getCardsOfHearts(HandOfCards hand) {
    StringBuilder hearts = new StringBuilder();
    ArrayList<PlayingCard> cards = new ArrayList<>();
    hand.getCards().stream().filter(card -> card.getSuit() == 'H').forEach(cards::add);
    cards.sort(Comparator.comparingInt(PlayingCard::getFace));
    cards.forEach(card -> hearts.append(card.getAsString()).append(" "));
    return hearts.toString();
    }
  
  /**
   * Checks if hand has queen of spades.
   *
   * @param hand HandOfCards
   * @return String - Returns "Yes" if hand has queen og spades, returns "No" if not
   */
  public String getQueenOfSpades(HandOfCards hand) {
    if (hand.getCards().size() == 0) {
      return "No";
    }
    if (hand.getCards().stream().anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12)) {
      return "Yes";
    } else {
      return "No";
    }
  }
  
  /**
   * Plays movement transition from the deck of cards to the hand.
   *
   * @param transitionToHandCard1 TranslateTransition - transition for card1
   * @param transitionToHandCard2 TranslateTransition - transition for card2
   * @param transitionToHandCard3 TranslateTransition - transition for card3
   * @param transitionToHandCard4 TranslateTransition - transition for card4
   * @param transitionToHandCard5 TranslateTransition - transition for card5
   */
  public void transitionFromDeckToHand(TranslateTransition transitionToHandCard1, TranslateTransition transitionToHandCard2,
                                       TranslateTransition transitionToHandCard3, TranslateTransition transitionToHandCard4,
                                       TranslateTransition transitionToHandCard5) {
    
    transitionToHandCard1.setToX(-200);
    transitionToHandCard1.setToY(390);
    transitionToHandCard2.setToX(-100);
    transitionToHandCard2.setToY(390);
    transitionToHandCard3.setToX(0);
    transitionToHandCard3.setToY(390);
    transitionToHandCard4.setToX(100);
    transitionToHandCard4.setToY(390);
    transitionToHandCard5.setToX(200);
    transitionToHandCard5.setToY(390);
  
    transitionToHandCard1.play();
    transitionToHandCard2.play();
    transitionToHandCard3.play();
    transitionToHandCard4.play();
    transitionToHandCard5.play();
  }
  
  /**
   * Plays movement transition from the hand to the deck of cards.
   *
   * @param transitionToHandCard1 TranslateTransition - transition for card1
   * @param transitionToHandCard2 TranslateTransition - transition for card2
   * @param transitionToHandCard3 TranslateTransition - transition for card3
   * @param transitionToHandCard4 TranslateTransition - transition for card4
   * @param transitionToHandCard5 TranslateTransition - transition for card5
   */
  public void transitionFromHandToDeck(TranslateTransition transitionToHandCard1, TranslateTransition transitionToHandCard2,
                                       TranslateTransition transitionToHandCard3, TranslateTransition transitionToHandCard4,
                                       TranslateTransition transitionToHandCard5) {
    transitionToHandCard1.setToX(200);
    transitionToHandCard1.setToY(-390);
    transitionToHandCard2.setToX(100);
    transitionToHandCard2.setToY(-390);
    transitionToHandCard3.setToX(0);
    transitionToHandCard3.setToY(-390);
    transitionToHandCard4.setToX(-100);
    transitionToHandCard4.setToY(-390);
    transitionToHandCard5.setToX(-200);
    transitionToHandCard5.setToY(-390);
    
    transitionToHandCard1.play();
    transitionToHandCard2.play();
    transitionToHandCard3.play();
    transitionToHandCard4.play();
    transitionToHandCard5.play();
  }
  
  /**
   * Plays the spin transition for cards.
   *
   * @param rotateTransitionCard1 rotateTransitionCard1 - rotation for card1
   * @param rotateTransitionCard2 rotateTransitionCard2 - rotation for card2
   * @param rotateTransitionCard3 rotateTransitionCard3 - rotation for card3
   * @param rotateTransitionCard4 rotateTransitionCard4 - rotation for card4
   * @param rotateTransitionCard5 rotateTransitionCard5 - rotation for card5
   */
  public void spinCards(RotateTransition rotateTransitionCard1,
                        RotateTransition rotateTransitionCard2, RotateTransition rotateTransitionCard3,
                        RotateTransition rotateTransitionCard4, RotateTransition rotateTransitionCard5) {
  
    List.of(rotateTransitionCard1, rotateTransitionCard2, rotateTransitionCard3,
            rotateTransitionCard4, rotateTransitionCard5).forEach(rotate -> {
      rotate.setByAngle(180);
      rotate.play();
    });
  }
  
  /**
   * Executes all actions when the button startOver is pushed.
   *
   * @param cards DeckOfCards
   * @param sumOfFaces Label
   * @param cardsLeft Label
   * @param hand HandOfCards
   * @param noMoreCards Label
   * @param transitionToHandCard1 TranslateTransition
   * @param transitionToHandCard2 TranslateTransition
   * @param transitionToHandCard3 TranslateTransition
   * @param transitionToHandCard4 TranslateTransition
   * @param transitionToHandCard5 TranslateTransition
   */
  public void startOver(DeckOfCards cards, Label sumOfFaces, Label cardsLeft, HandOfCards hand,
                        Label noMoreCards, TranslateTransition transitionToHandCard1,
                        TranslateTransition transitionToHandCard2, TranslateTransition transitionToHandCard3,
                        TranslateTransition transitionToHandCard4, TranslateTransition transitionToHandCard5) {
  
    handGiven = false;
    dealCards = false;
    cards.retrieveAllCards();
    sumOfFaces.setText("Sum of faces: 0");
    cardsLeft.setText("Cards left: " + cards.getCards().size());
    hand.emptyHand();
    noMoreCards.setTranslateX(0);
    noMoreCards.setTranslateY(-500);
    
    transitionFromHandToDeck(transitionToHandCard1, transitionToHandCard2,
            transitionToHandCard3, transitionToHandCard4, transitionToHandCard5);
  }
  
  /**
   * Executes all actions when the button checkHand is pushed.
   *
   * @param cardsLeft Label
   * @param cards DeckOfCards
   * @param noMoreCards Label
   * @param hand HandOfCards
   * @param sumOfFaces Label
   * @param flushLabel Label
   * @param cardsOfHearts Label
   * @param queenOfSpades Label
   * @param card1 ImageView
   * @param card2 ImageView
   * @param card3 ImageView
   * @param card4 ImageView
   * @param card5 ImageView
   * @param rotateTransitionCard1 RotateTransition
   * @param rotateTransitionCard2 RotateTransition
   * @param rotateTransitionCard3 RotateTransition
   * @param rotateTransitionCard4 RotateTransition
   * @param rotateTransitionCard5 RotateTransition
   */
  public void checkHand(Label cardsLeft, DeckOfCards cards, Label noMoreCards,
                        HandOfCards hand, Label sumOfFaces, Label flushLabel, Label cardsOfHearts,
                        Label queenOfSpades, ImageView card1, ImageView card2, ImageView card3,
                        ImageView card4, ImageView card5, RotateTransition rotateTransitionCard1,
                        RotateTransition rotateTransitionCard2, RotateTransition rotateTransitionCard3,
                        RotateTransition rotateTransitionCard4, RotateTransition rotateTransitionCard5) {
    
    cardsLeft.setText("Cards left: " + cards.getCards().size());
    handGiven = false;
    if (cards.getCards().size() < 5) {
      noMoreCards.setTranslateX(0);
      noMoreCards.setTranslateY(500);
    } else if (dealCards) {
      hand.pickNewHand(cards.dealHand(5));
      sumOfFaces.setText("Sum of faces: " + getSumOfFaces(hand));
      flushLabel.setText("Flush: " + flush(hand));
      cardsOfHearts.setText("Cards of hearts: " + getCardsOfHearts(hand));
      queenOfSpades.setText("Queen of spades: " + getQueenOfSpades(hand));
  
      cardsLeft.setText("Cards left: " + cards.getCards().size());
      List.of(card1, card2, card3, card4, card5).forEach(card -> card.setImage(getCardImage("Backside")));
      
      spinCards(rotateTransitionCard1, rotateTransitionCard2, rotateTransitionCard3,
              rotateTransitionCard4, rotateTransitionCard5);
      
      card1.setImage(getCardImage(hand.getCards().get(0).getAsString()));
      card2.setImage(getCardImage(hand.getCards().get(1).getAsString()));
      card3.setImage(getCardImage(hand.getCards().get(2).getAsString()));
      card4.setImage(getCardImage(hand.getCards().get(3).getAsString()));
      card5.setImage(getCardImage(hand.getCards().get(4).getAsString()));
    }
  }
  
  /**
   * Executes all actions when the button dealHAnd is pushed.
   *
   * @param hand HandOfCards
   * @param cardsLeft Label
   * @param cards DeckOfCards
   * @param card1 ImageView
   * @param card2 ImageView
   * @param card3 ImageView
   * @param card4 ImageView
   * @param card5 ImageView
   * @param transitionToHandCard1 TranslateTransition
   * @param transitionToHandCard2 TranslateTransition
   * @param transitionToHandCard3 TranslateTransition
   * @param transitionToHandCard4 TranslateTransition
   * @param transitionToHandCard5 TranslateTransition
   */
  public void dealHand(HandOfCards hand, Label cardsLeft, DeckOfCards cards,
                        ImageView card1, ImageView card2, ImageView card3,
                        ImageView card4, ImageView card5, TranslateTransition transitionToHandCard1,
                       TranslateTransition transitionToHandCard2, TranslateTransition transitionToHandCard3,
                       TranslateTransition transitionToHandCard4, TranslateTransition transitionToHandCard5) {
    
    dealCards = true;
    cardsLeft.setText("Cards left: " + cards.getCards().size());
    List.of(card1, card2, card3, card4, card5).forEach(card -> card.setImage(getCardImage("Backside")));
    if (!handGiven) {
      card1.setTranslateX(200);
      card1.setTranslateY(-390);
      card2.setTranslateX(100);
      card2.setTranslateY(-390);
      card3.setTranslateX(0);
      card3.setTranslateY(-390);
      card4.setTranslateX(-100);
      card4.setTranslateY(-390);
      card5.setTranslateX(-200);
      card5.setTranslateY(-390);
      handGiven = true;
    }
    transitionFromDeckToHand(transitionToHandCard1, transitionToHandCard2,
            transitionToHandCard3, transitionToHandCard4, transitionToHandCard5);
  }
}
